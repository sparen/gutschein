# README #

Um einen Gutschein im Onlineshop deiner Wahl einzulösen, gehe wie folgt vor:

1. Suche für deinen Onlineshop einen passenden Gutschein auf https://www.bummelwelt.de

2. Klicke den Gutschein an, indem du auf "Gutschein einlösen" oder "Gutscheincode anzeigen" klickst.

3. Nun wirst du entweder zum aktiven Rabatt im Onlineshop weitergeleitet, oder es wird dir ein Gutscheincode angezeigt.

4. Falls du zum rabattierten Produkt geleitet wurdest, lege es in den Warenkorb und schließe die Bestellung an der Kasse wie gewohnt ab. 
	Glückwunsch: Du hast gerade ein Schnäppchen gemacht! :-)
	
5. Wenn dir ein Gutscheincode auf Bummelwelt.de angezeigt wird, kopiere ihn in die Zwischenablage, indem du auf "Copy" klickst, oder notiere ihn dir.
	Wechsle nun zum Onlineshop und lege dort alle gewünschten Artikel in den Warenkorb. Wechsle dorthin. 
	Gib nun den Gutscheincode von Bummelwelt in ein Feld ein, das mit "Gutschein" oder ähnlich beschriftet ist und aktiviere den Gutscheincode.
	Der Rabatt wird dir nun angezeigt. Schließe die Bestellung ab. Du hast den angegeben Geldbetrag gespart. :-)
